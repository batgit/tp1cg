// Comando para compilação: gcc tp1.c -lSOIL -lglut -lGL -lGLEW -lGLU -lm -lSDL2_mixer -lSDL2

// Versão: 1.0
#include <SDL2/SDL.h> // Músicaaaa.
#include <SDL2/SDL_mixer.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <SOIL/SOIL.h>
#include <time.h>

// Path das nossas músicas.
#define MUS_SKYRIM_PATH "sounds/skyrim.mpeg"
#define MUS_GAME_PATH "sounds/game_of_thrones.mpeg"

// Ponteiro para a música.
Mix_Music *musicGame = NULL;

// Variáveis de textura
GLuint REIDANOITE;
GLuint MURALHA;
GLuint CHAO;

//Texturas das comidas
GLuint BENJEN;
GLuint OBERYN;
GLuint NED;
GLuint JEOR;
GLuint DROGO;
GLuint MINDINHO;
GLuint OLLY;
GLuint ROBB;
GLuint ROBERT;
GLuint TYWIN;
GLuint YOUKNOWNOTHING;
GLuint VISERYS;
GLuint RAMSAY;
//Texturas dos colecionaveis
GLuint CORRENTES;
GLuint GHOST;
GLuint LANCADEGELO;
GLuint LEAF;
GLuint HODOR;
GLuint VISERION;

//texturas das telas
GLuint TELAPAUSE;
GLuint TELAREINICIAR;
GLuint TELASAIR;
GLuint SCORE;
GLuint MENUPRINCIPAL;
GLuint MENUPRINCIPALOPT; //"opcoes" fica selecionado
GLuint MENUPRINCIPALST; //"story mode " fica selecionado
GLuint MENUPRINCIPALTT;//"time trial" fica selecionado
GLuint MENUPRINCIPALHTP;//"instrucoes" fica selecionado
GLuint HOWTOPLAY;
GLuint CREDITOS;
GLuint RETURNTOMENU; //tela de confirmacao ao voltar para o menu principal
GLuint GAMEOVER;
GLuint ENDSTORY; //tela que
GLuint TIME;


// Variaveis de tempo.
// Uma para armazenar o valor inicial e outra que irá guardar a variacao.
time_t inicioTempoJogo, deltaTempo, inicioTempoParadinhas ,deltaTempoParadinhas;
int m, s, tempoParadinhas; // Minutos e segundos do Jogo.

// Estrutura para definir o tamanho do mundo.
typedef struct{
    int x;
    int y;
    int zmin;
    int zmax;
}tammundo;

// Estrutura para definição da comida.
typedef struct {
    int largura;
    int altura;
    int x;
    int y;
    int estado; // Variável de estado da comida. 1 não comido e 0 comido.
}comida;

// Estrutura para definição do personagem.
typedef struct {
  int largura;
  int altura;
  float x;
  float y;
  int estadoMovimento;
  float transparencia;
}rn;

//Estrutura para as telas.
typedef struct{
    int x;
    int y;
    int largura;
    int altura;
    float transparencia;
}tela;

//Estrutura para registrar o movimento do mouse
struct ponto {
    float x, y;
};

struct ponto posicaoMouse;

//tela modo de jogo Story Mode
tela telaStoryMode;
//tela modo de jogo Time Trial
tela telaTimeTrial;
//tela de instrucoes
tela telaHowToPlay;
//tela dos creditos
tela telaCreditos;
// variavel da tela de pontos
tela telaPontos;
// tela de pontos da historia
tela telaPontosStory;
// variavel da tela de pause
tela telaPause;
// variavel da tela de reiniciar
tela telaReiniciar;
// variavel do tamanho maximo do mundo
tammundo tamanhomaximo;
// Variável da comida.
comida c;
// Variável do personagem jogavel. Padrão: reiDaNoite.
rn reiDaNoite;

int score = -1; //quantidade de pontos. comeca com -1 para que a primeira vez que o jogo rode, apareca 0.
int scoreLancas = 0; //variavel usada para printar na tela quantas lancas o player tem no StoryMode
int scoreCorrentes = 0; //variavel usada para printar na tela quantas correntes o player tem no StoryMode
char num[10]; //este vetor é usado para printar em tempo de jogo a pontuacao
//estes dois vetores sao usados para printar em tempo de jogo a pontuacao no Story Mode
char min[2];
char sec[2];

int larguraBarreiras = 7;
// Esta variavel vai controlar se o player atingiu ou não uma das barreiras.
// O valor será alterado quando ao desenhar a cena a coordenadas do player coincidirem com a barreira.
int controlaLimite=1;
// controlaPausar irá mudar de estado se o botão p for pressionado.
// Pausado = -1; Não Pausado = 1;
// Quando a tela é redesenhada conferimos se o estado dela é diferente de -1
int controlaPausar=1;
//controla o desenho da tela "deseja realmente reiniciar?"
int controlaReiniciar=1;
//Esta variavel irá controlar qual textura a ser aplicada na comida de cada vez. ela varia de 1 a 12, mas deve ser iniciada com 0,
//para que quando chegue na funcao pela primeira vez, ela seja acrescida de 1 e imprima a primeira textura.
int controleDeTexturaDasComidas=0;
//controla a tela "deseja realmente sair do jogo?"
int controlaSairJogo=0;
//quando o jogador terminar o modo historia essa variavel se alterará, terminando o modo historia
int controlaStory=0;
//controla o efeito de invisibilidade do personagem
int controlaInvisibilidade = -1;
int tempoDuracaoInvisibilidade = 5;
//controla o efeito de mais velocidade do personagem
int controlaSuperVelocidade = -1;
int tempoDuracaoSuperVelocidade = 5;
//controla o efeito de menos velocidade do personagem
int controlaSubVelocidade = -1;
int tempoDuracaoSubVelocidade = 5;
//variavel que controla o primeiro movimento do jogo para iniciar o relógio
int jogoComecou=0;

int telaAtualDoJogo=0;
/*
    tela 0 = menu inicial
    tela 1 = instrucoes
    tela 2 = creditos
    tela 3 = jogar (time trial)
    tela 4 = jogar (sotry mode)

*/
void escreveTexto(void * font, char *s, float x, float y, float z){
    int i;
    glRasterPos3f(x, y, z);
    // Renderização dos bitmaps de cada caracter da minha string.
    for (i=0; i < strlen(s); i++)
       glutBitmapCharacter(font, s[i]);
}

void desenhaPersonagens(){
    glBindTexture(GL_TEXTURE_2D, REIDANOITE);
    glEnable(GL_BLEND); // Habilitar a transparência.
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if(controlaInvisibilidade==1)
        reiDaNoite.transparencia=0.05;
    else
        reiDaNoite.transparencia=1;
        glColor4f(1,1,1,reiDaNoite.transparencia);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(reiDaNoite.x-reiDaNoite.largura,reiDaNoite.y-reiDaNoite.altura,-2);
        glTexCoord2f(1,0); glVertex3f(reiDaNoite.x,reiDaNoite.y-reiDaNoite.altura,-2);
        glTexCoord2f(1,1); glVertex3f(reiDaNoite.x,reiDaNoite.y,-2);
        glTexCoord2f(0,1); glVertex3f(reiDaNoite.x-reiDaNoite.largura,reiDaNoite.y,-2);
    glEnd();
    glDisable(GL_BLEND);
}

void desenhaBarreiras(){
    glColor3f(1,0,0);
    if(controlaLimite)
    glColor3f(1,1,1);
        glBindTexture(GL_TEXTURE_2D, CHAO);
    glBegin(GL_TRIANGLE_FAN);
      glTexCoord2f(0,0); glVertex3f(larguraBarreiras,larguraBarreiras,-2);
      glTexCoord2f(1,0); glVertex3f(100-larguraBarreiras,larguraBarreiras,-2);
      glTexCoord2f(1,1); glVertex3f(100-larguraBarreiras,100-larguraBarreiras,-2);
      glTexCoord2f(0,1); glVertex3f(larguraBarreiras,100-larguraBarreiras,-2);
    glEnd();
    // A partir daqui comeco a colocar textura nas bordas.
    glColor3f(1,1,1);
    if(controlaStory==1)
        glColor3f(1,0,0);

        glBindTexture(GL_TEXTURE_2D, MURALHA);
    glBegin(GL_TRIANGLE_FAN); // Borda direita
        glTexCoord2f(0,0); glVertex3f(tamanhomaximo.x-larguraBarreiras,0,-2);
        glTexCoord2f(1,0); glVertex3f(tamanhomaximo.x-larguraBarreiras,tamanhomaximo.y,-2);
        glTexCoord2f(1,1); glVertex3f(tamanhomaximo.x,tamanhomaximo.y,-2);
        glTexCoord2f(0,1); glVertex3f(tamanhomaximo.x,0,-2);
    glEnd();

    glBegin(GL_TRIANGLE_FAN); // Borda esquerda
        glTexCoord2f(0,1); glVertex3f(0,0,-2);
        glTexCoord2f(0,0); glVertex3f(larguraBarreiras,0,-2);
        glTexCoord2f(1,0); glVertex3f(larguraBarreiras,tamanhomaximo.y,-2);
        glTexCoord2f(1,1); glVertex3f(0,tamanhomaximo.y,-2);
    glEnd();

    glBegin(GL_TRIANGLE_FAN); // Borda superior
        glTexCoord2f(0,0); glVertex3f(larguraBarreiras,tamanhomaximo.y-larguraBarreiras,-2);
        glTexCoord2f(1,0); glVertex3f(tamanhomaximo.x,tamanhomaximo.y-larguraBarreiras,-2);
        glTexCoord2f(1,1); glVertex3f(tamanhomaximo.x,tamanhomaximo.y,-2);
        glTexCoord2f(0,1); glVertex3f(larguraBarreiras,tamanhomaximo.y,-2);
    glEnd();

    glBegin(GL_TRIANGLE_FAN); // Borda inferior
        glTexCoord2f(0,0); glVertex3f(tamanhomaximo.x-larguraBarreiras,larguraBarreiras,-2);
        glTexCoord2f(1,0); glVertex3f(larguraBarreiras,larguraBarreiras,-2);
        glTexCoord2f(1,1); glVertex3f(larguraBarreiras,0,-2);
        glTexCoord2f(0,1); glVertex3f(tamanhomaximo.x-larguraBarreiras,0,-2);
    glEnd();

}

void desenhaTelaPause(){
    glEnable(GL_BLEND); // Habilitar a transparência.
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1,1,1,telaPause.transparencia);
    glBindTexture(GL_TEXTURE_2D, TELAPAUSE);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(telaPause.x-telaPause.largura/2,telaPause.y-telaPause.altura/2,-2);
        glTexCoord2f(1,0); glVertex3f(telaPause.x+telaPause.largura/2,telaPause.y-telaPause.altura/2,-2);
        glTexCoord2f(1,1); glVertex3f(telaPause.x+telaPause.largura/2,telaPause.y+telaPause.altura/2,-2);
        glTexCoord2f(0,1); glVertex3f(telaPause.x-telaPause.largura/2,telaPause.y+telaPause.altura/2,-2);;
    glEnd();
    glDisable(GL_BLEND);
}
//esta tela desenha os minutos e segundos decorridos no Story Mode.
void desenhaTelaPontosStory(){
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glColor4f(0.8,0.8,0.8,telaPontosStory.transparencia);

    glBindTexture(GL_TEXTURE_2D, TIME);//desenha a tela que fica escrito "min  sec" durante o modo historia
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(telaPontosStory.x-telaPontosStory.largura/2,telaPontosStory.y-telaPontosStory.altura/2,-2);
        glTexCoord2f(1,0); glVertex3f(telaPontosStory.x+telaPontosStory.largura/2,telaPontosStory.y-telaPontosStory.altura/2,-2);
        glTexCoord2f(1,1); glVertex3f(telaPontosStory.x+telaPontosStory.largura/2,telaPontosStory.y+telaPontosStory.altura/2,-2);
        glTexCoord2f(0,1); glVertex3f(telaPontosStory.x-telaPontosStory.largura/2,telaPontosStory.y+telaPontosStory.altura/2,-2);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, LANCADEGELO); //coloca o desenho da lanca de gelo para o player ter controle de
    glBegin(GL_TRIANGLE_FAN);//quantas lancas ja tem
        glTexCoord2f(0,0); glVertex3f(41-c.largura/2,telaPontosStory.y-c.altura/2+2,-2);
        glTexCoord2f(1,0); glVertex3f(41+c.largura/2,telaPontosStory.y-c.altura/2+2,-2);
        glTexCoord2f(1,1); glVertex3f(41+c.largura/2,telaPontosStory.y+c.altura/2+2,-2);
        glTexCoord2f(0,1); glVertex3f(41-c.largura/2,telaPontosStory.y+c.altura/2+2,-2);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, CORRENTES); //coloca o desenho das correntes para o player ter controle de
    glBegin(GL_TRIANGLE_FAN); //quantas correntes ja tem
        glTexCoord2f(0,0); glVertex3f(56-c.largura/2,telaPontosStory.y-c.altura/2+2,-2);
        glTexCoord2f(1,0); glVertex3f(56+c.largura/2,telaPontosStory.y-c.altura/2+2,-2);
        glTexCoord2f(1,1); glVertex3f(56+c.largura/2,telaPontosStory.y+c.altura/2+2,-2);
        glTexCoord2f(0,1); glVertex3f(56-c.largura/2,telaPontosStory.y+c.altura/2+2,-2);
    glEnd();
    glDisable(GL_BLEND);

    glColor3f (0, 0, 0);
    sprintf(min, "%.2d", m);
   escreveTexto(GLUT_BITMAP_TIMES_ROMAN_24, min, 84, 95, -2);
   sprintf(sec, "%.2d", s);
   escreveTexto(GLUT_BITMAP_TIMES_ROMAN_24, sec, 92, 95, -2);
   sprintf(num, "%.1d", scoreLancas);
   escreveTexto(GLUT_BITMAP_TIMES_ROMAN_24, num, 45, 95, -2);
   escreveTexto(GLUT_BITMAP_TIMES_ROMAN_24, "/3", 46, 95, -2);
   sprintf(num, "%.1d", scoreCorrentes);
   escreveTexto(GLUT_BITMAP_TIMES_ROMAN_24, num, 60, 95, -2);
   escreveTexto(GLUT_BITMAP_TIMES_ROMAN_24, "/2", 61, 95, -2);
   glColor3f (1, 0, 0);
   if(controlaSubVelocidade==1 || controlaSuperVelocidade==1 || controlaInvisibilidade==1)
   {
       sprintf(sec, "%.2d", 5-tempoParadinhas);
       escreveTexto(GLUT_BITMAP_TIMES_ROMAN_24, sec, 8, 95, -2);
   }
}
//desenha a pontuacao no Time Trial
void desenhaTelaPontos(){
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(0.8,0.8,0.8,telaPontos.transparencia);
    glBindTexture(GL_TEXTURE_2D, SCORE);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(telaPontos.x-telaPontos.largura/2,telaPontos.y-telaPontos.altura/2,-2);
        glTexCoord2f(1,0); glVertex3f(telaPontos.x+telaPontos.largura/2,telaPontos.y-telaPontos.altura/2,-2);
        glTexCoord2f(1,1); glVertex3f(telaPontos.x+telaPontos.largura/2,telaPontos.y+telaPontos.altura/2,-2);
        glTexCoord2f(0,1); glVertex3f(telaPontos.x-telaPontos.largura/2,telaPontos.y+telaPontos.altura/2,-2);;
    glEnd();
    glDisable(GL_BLEND);
    glColor3f (0, 0, 0);
    sprintf(num, "%.3d", score);
   escreveTexto(GLUT_BITMAP_TIMES_ROMAN_24, num, 93, 95, -2);
    glColor3f (1, 0, 0);
    sprintf(sec, "%.2d", 31-s);
   escreveTexto(GLUT_BITMAP_TIMES_ROMAN_24, sec, 8, 95, -2);
   if(controlaSubVelocidade==1 || controlaSuperVelocidade==1 || controlaInvisibilidade==1)
   {
       sprintf(sec, "%.2d", 5-tempoParadinhas);
       escreveTexto(GLUT_BITMAP_TIMES_ROMAN_24, sec, 15, 95, -2);
   }
}
//desenha a tela "deseja mesmo reiniciar?"
void desenhaTelaReiniciar(){
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1,1,1,telaReiniciar.transparencia);
    glBindTexture(GL_TEXTURE_2D,TELAREINICIAR);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(telaReiniciar.x-telaReiniciar.largura/2,telaReiniciar.y-telaReiniciar.altura/2,-2);
        glTexCoord2f(1,0); glVertex3f(telaReiniciar.x+telaReiniciar.largura/2,telaReiniciar.y-telaReiniciar.altura/2,-2);
        glTexCoord2f(1,1); glVertex3f(telaReiniciar.x+telaReiniciar.largura/2,telaReiniciar.y+telaReiniciar.altura/2,-2);
        glTexCoord2f(0,1); glVertex3f(telaReiniciar.x-telaReiniciar.largura/2,telaReiniciar.y+telaReiniciar.altura/2,-2);
    glEnd();
    glDisable(GL_BLEND);
}

GLuint selecionaTexturaComida() // Esta funcao seleciona qual será a textura aplicada na comida. ela retorna um GLuint.
{
    if (controleDeTexturaDasComidas==1)
        return BENJEN;
    if(controleDeTexturaDasComidas==2)
        return OBERYN;
    if(controleDeTexturaDasComidas==3)
        return NED;
    if(controleDeTexturaDasComidas==4)
        return JEOR;
    if(controleDeTexturaDasComidas==5)
        return DROGO;
    if(controleDeTexturaDasComidas==6)
        return MINDINHO;
    if(controleDeTexturaDasComidas==7)
        return OLLY;
    if(controleDeTexturaDasComidas==8)
        return ROBB;
    if(controleDeTexturaDasComidas==9)
        return ROBERT;
    if(controleDeTexturaDasComidas==10)
        return TYWIN;
    if(controleDeTexturaDasComidas==11)
        return YOUKNOWNOTHING;
    if(controleDeTexturaDasComidas==12)
        return VISERYS;
    if(controleDeTexturaDasComidas==13)
        return HODOR;
    if(controleDeTexturaDasComidas==14)
        return GHOST;
    if(controleDeTexturaDasComidas==15)
        return LEAF;

}

// Função para desenhar a comida.
void desenhaComida(){
    glColor3f(1.0,1.0,1.0);
    // Posição da comida.
    if(c.estado==1){
        score++;
        if(score%5==0){
            reiDaNoite.largura++;
            reiDaNoite.altura++;
        }
        c.x=(rand()%81)+15;
        c.y=(rand()%81)+15;
        c.estado=0;
        if(controleDeTexturaDasComidas==13)
        {
            controlaSubVelocidade=controlaSubVelocidade*(-1);
            inicioTempoParadinhas=time(NULL);
        }
        if(controleDeTexturaDasComidas==14)
        {
            controlaSuperVelocidade=controlaSuperVelocidade*(-1);
            inicioTempoParadinhas=time(NULL);
        }
        if(controleDeTexturaDasComidas==15)
        {
            controlaInvisibilidade=controlaInvisibilidade*(-1);
            inicioTempoParadinhas=time(NULL);
        }
        controleDeTexturaDasComidas=1+rand()%15;
        glutPostRedisplay();
    }
        glBindTexture(GL_TEXTURE_2D,selecionaTexturaComida());
        glEnable(GL_BLEND); // Habilitar a transparência.
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(c.x-c.largura, c.y-c.altura, -3);
        glTexCoord2f(1,0); glVertex3f(c.x, c.y-c.altura, -3);
        glTexCoord2f(1,1); glVertex3f(c.x, c.y, -3);
        glTexCoord2f(0,1); glVertex3f(c.x-c.largura, c.y, -3);
    glEnd();
    glDisable(GL_BLEND);
}
//esta funcao verifica a colisao
void setEstadoComida(){
    if (reiDaNoite.x>=c.x && reiDaNoite.x-reiDaNoite.largura>=c.x-c.largura && reiDaNoite.x-reiDaNoite.largura<=c.x || reiDaNoite.x-reiDaNoite.largura<=c.x-c.largura && reiDaNoite.x<=c.x && reiDaNoite.x>=c.x-c.largura)
        if(reiDaNoite.y>=c.y && reiDaNoite.y-reiDaNoite.altura>=c.y-c.altura && reiDaNoite.y-reiDaNoite.altura<=c.y || reiDaNoite.y-reiDaNoite.altura<=c.y-c.altura && reiDaNoite.y<=c.y && reiDaNoite.y>=c.y-c.altura || reiDaNoite.y>=c.y && reiDaNoite.y-reiDaNoite.altura<=c.y-c.altura)
            c.estado=1;
    if(reiDaNoite.y>=c.y && reiDaNoite.y-reiDaNoite.altura>=c.y-c.altura && reiDaNoite.y-reiDaNoite.altura<=c.y || reiDaNoite.y-reiDaNoite.altura<=c.y-c.altura && reiDaNoite.y<=c.y && reiDaNoite.y>=c.y-c.altura)
        if (reiDaNoite.x>=c.x && reiDaNoite.x-reiDaNoite.largura>=c.x-c.largura && reiDaNoite.x-reiDaNoite.largura<=c.x || reiDaNoite.x-reiDaNoite.largura<=c.x-c.largura && reiDaNoite.x<=c.x && reiDaNoite.x>=c.x-c.largura || reiDaNoite.x>=c.x && reiDaNoite.x-reiDaNoite.largura<=c.x-c.largura)
            c.estado=1;
    desenhaComida();
}

//desenha a tela "deseja realmente sair?"
void desenhaTelaSair(){
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1,1,1,1);
    glBindTexture(GL_TEXTURE_2D,TELASAIR);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(telaReiniciar.x-telaReiniciar.largura/2,telaReiniciar.y-telaReiniciar.altura/2,-2);
        glTexCoord2f(1,0); glVertex3f(telaReiniciar.x+telaReiniciar.largura/2,telaReiniciar.y-telaReiniciar.altura/2,-2);
        glTexCoord2f(1,1); glVertex3f(telaReiniciar.x+telaReiniciar.largura/2,telaReiniciar.y+telaReiniciar.altura/2,-2);
        glTexCoord2f(0,1); glVertex3f(telaReiniciar.x-telaReiniciar.largura/2,telaReiniciar.y+telaReiniciar.altura/2,-2);
    glEnd();
    glDisable(GL_BLEND);
}

void reinicia()
{
    inicioTempoJogo = time(NULL);
    controlaReiniciar=1;
    controlaPausar=1;
    reiDaNoite.estadoMovimento=0;
    reiDaNoite.x=50;
    reiDaNoite.y=50;
    reiDaNoite.largura=6;
    reiDaNoite.altura=6;
    controleDeTexturaDasComidas=1;
    controlaLimite=1;
    c.estado=1;
    score=-1;
    scoreLancas=0;
    scoreCorrentes=0;
    controlaSubVelocidade=-1;
    controlaSuperVelocidade=-1;
    controlaInvisibilidade=-1;
    jogoComecou=0;
}

void desenhaMenuPrincipal()
{
    /*
        tela 0 = menu inicial
        tela 1 = instrucoes
        tela 2 = creditos
        tela 3 = jogar (time trial)
        tela 4 = jogar (story mode)
    */
    //if(jogoComecou)

    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1,1,1,1);

    if(telaAtualDoJogo==0)//se o jogo estiver no menu, verifica o movimento do mouse e altera a textura se o
                          //mouse chegar em algum dos botoes
    {
        if(posicaoMouse.x>=telaStoryMode.x && posicaoMouse.y >=telaStoryMode.y && posicaoMouse.x <= telaStoryMode.x+telaStoryMode.largura && posicaoMouse.y<= telaStoryMode.y+telaStoryMode.altura)
        glBindTexture(GL_TEXTURE_2D,MENUPRINCIPALST);
        else if (posicaoMouse.x>=telaTimeTrial.x && posicaoMouse.y >=telaTimeTrial.y && posicaoMouse.x <= telaTimeTrial.x+telaTimeTrial.largura && posicaoMouse.y<= telaTimeTrial.y+telaTimeTrial.altura)
        glBindTexture(GL_TEXTURE_2D,MENUPRINCIPALTT);
        else if (posicaoMouse.x>=telaHowToPlay.x && posicaoMouse.y >=telaHowToPlay.y && posicaoMouse.x <= telaHowToPlay.x+telaHowToPlay.largura && posicaoMouse.y<= telaHowToPlay.y+telaHowToPlay.altura)
        glBindTexture(GL_TEXTURE_2D,MENUPRINCIPALHTP);
        else if (posicaoMouse.x>=telaCreditos.x && posicaoMouse.y >=telaCreditos.y && posicaoMouse.x <= telaCreditos.x+telaCreditos.largura && posicaoMouse.y<= telaCreditos.y+telaCreditos.altura)
        glBindTexture(GL_TEXTURE_2D,MENUPRINCIPALOPT);
        else
        glBindTexture(GL_TEXTURE_2D,MENUPRINCIPAL);
    }
    else if(telaAtualDoJogo==3 || telaAtualDoJogo==4) //se o player estiver jogando e pressionar M, este else if
                                                    //desenha a tela "deseja voltar ao menu principal?"
        glBindTexture(GL_TEXTURE_2D,RETURNTOMENU);
        glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(0,0,-2);
        glTexCoord2f(1,0); glVertex3f(tamanhomaximo.x,0,-2);
        glTexCoord2f(1,1); glVertex3f(tamanhomaximo.x,tamanhomaximo.y,-2);
        glTexCoord2f(0,1); glVertex3f(0,tamanhomaximo.y,-2);
    glEnd();
    glDisable(GL_BLEND);

    if(controlaSairJogo) //se o player apertar ESC este if desenha a tela "deseja sair do jogo?"
        desenhaTelaSair();

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glutSwapBuffers();
}
//desenha a tela de creditos
void desenhaCreditos()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1,1,1,1);
    glBindTexture(GL_TEXTURE_2D,CREDITOS);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(0,0,-2);
        glTexCoord2f(1,0); glVertex3f(tamanhomaximo.x,0,-2);
        glTexCoord2f(1,1); glVertex3f(tamanhomaximo.x,tamanhomaximo.y,-2);
        glTexCoord2f(0,1); glVertex3f(0,tamanhomaximo.y,-2);
    glEnd();
    glDisable(GL_BLEND);

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glutSwapBuffers();
}
//desenha a tela de instrucoes
void desenhaInstrucoes()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1,1,1,1);
    glBindTexture(GL_TEXTURE_2D,HOWTOPLAY);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(0,0,-2);
        glTexCoord2f(1,0); glVertex3f(tamanhomaximo.x,0,-2);
        glTexCoord2f(1,1); glVertex3f(tamanhomaximo.x,tamanhomaximo.y,-2);
        glTexCoord2f(0,1); glVertex3f(0,tamanhomaximo.y,-2);
    glEnd();
    glDisable(GL_BLEND);

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glutSwapBuffers();
}
//desenha a tela de fim de jogo
void desenhaGameOver()
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1,1,1,1);
    glBindTexture(GL_TEXTURE_2D,GAMEOVER);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(0,0,-2);
        glTexCoord2f(1,0); glVertex3f(tamanhomaximo.x,0,-2);
        glTexCoord2f(1,1); glVertex3f(tamanhomaximo.x,tamanhomaximo.y,-2);
        glTexCoord2f(0,1); glVertex3f(0,tamanhomaximo.y,-2);
    glEnd();
    glDisable(GL_BLEND);
}
//desenha o modo de jogo Time Trial
void desenhaTimeTrial(){
    // Comparação das barreiras.
    // Esta linha compara a posicao (x,y) do player com as coordenadas da largura da barreira. se alaguma das coordenadas coincidir, o jogo para
    if(reiDaNoite.x<=(larguraBarreiras+reiDaNoite.largura) || reiDaNoite.x>=(100-larguraBarreiras) || reiDaNoite.y<=(larguraBarreiras+reiDaNoite.altura) || reiDaNoite.y>=(100-larguraBarreiras))
        controlaLimite=0;
    if(s==30)
        controlaLimite=0;

    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

        desenhaBarreiras();
        desenhaTelaPontos(); // Tela de Pontos.
        desenhaPersonagens();
        setEstadoComida();

        if(controlaPausar==(-1))
            desenhaTelaPause();
        if(controlaReiniciar==(-1))
            desenhaTelaReiniciar();
        if(controlaSairJogo)
            desenhaTelaSair();
        if(controlaLimite==0)
            desenhaGameOver();


    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glutSwapBuffers();
}
//esta funcao deve ser diferente da selecionaTexturaComida comum, porque deve verificar antes de tudo a pontuacao
//do jogador, para spawnar os itens especiais e unicos do modo historia
GLuint selecionaTexturaComidaStory() // Esta funcao seleciona qual será a textura aplicada na comida. ela retorna um GLuint.
{
    if(score==15)
        return LANCADEGELO;
    else if(score==30)//30
        return CORRENTES;
    else if(score==40)//40
        return LANCADEGELO;
    else if(score==50)//50
        return CORRENTES;
    else if(score==55)//55
        return LANCADEGELO;
    else if(score==60)//60
        return VISERION;

    else if (controleDeTexturaDasComidas==1)
        return BENJEN;
    else if(controleDeTexturaDasComidas==2)
        return OBERYN;
    else if(controleDeTexturaDasComidas==3)
        return NED;
    else if(controleDeTexturaDasComidas==4)
        return JEOR;
    else if(controleDeTexturaDasComidas==5)
        return DROGO;
    else if(controleDeTexturaDasComidas==6)
        return MINDINHO;
    else if(controleDeTexturaDasComidas==7)
        return OLLY;
    else if(controleDeTexturaDasComidas==8)
        return ROBB;
    else if(controleDeTexturaDasComidas==9)
        return ROBERT;
    else if(controleDeTexturaDasComidas==10)
        return TYWIN;
    else if(controleDeTexturaDasComidas==11)
        return YOUKNOWNOTHING;
    else if(controleDeTexturaDasComidas==12)
        return VISERYS;
    else if(controleDeTexturaDasComidas==13)
        return HODOR;
    else if(controleDeTexturaDasComidas==14)
        return GHOST;
    else if(controleDeTexturaDasComidas==15)
        return LEAF;
}
//esta funcao deve ser diferente do desenhaComida comum, porque quando a pontuacao chega a 60, ela retorna
//a comida que termina o modo historia
void desenhaComidaStory(){
    glColor3f(1.0,1.0,1.0);
    if(c.estado==1){
        if(score==15)
            scoreLancas++;
        else if(score==30)//30
            scoreCorrentes++;
        else if(score==40)//40
            scoreLancas++;
        else if(score==50)//50
            scoreCorrentes++;
        else if(score==55)//55
            scoreLancas++;
        else if(score==60)//60
            controlaStory=1;
        score++;
        if(score%5==0){
            reiDaNoite.largura++;
            reiDaNoite.altura++;
        }
        c.x=(rand()%81)+15;
        c.y=(rand()%81)+15;
        c.estado=0;
        if(controleDeTexturaDasComidas==13)
        {
            controlaSubVelocidade=controlaSubVelocidade*(-1);
            inicioTempoParadinhas=time(NULL);
        }
        if(controleDeTexturaDasComidas==14)
        {
            controlaSuperVelocidade=controlaSuperVelocidade*(-1);
            inicioTempoParadinhas=time(NULL);
        }
        if(controleDeTexturaDasComidas==15)
        {
            controlaInvisibilidade=controlaInvisibilidade*(-1);
            inicioTempoParadinhas=time(NULL);
        }
        controleDeTexturaDasComidas=1+rand()%15;
        glutPostRedisplay();
    }
        glBindTexture(GL_TEXTURE_2D,selecionaTexturaComidaStory());
        glEnable(GL_BLEND); // Habilitar a transparência.
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(c.x-c.largura, c.y-c.altura, -3);
        glTexCoord2f(1,0); glVertex3f(c.x, c.y-c.altura, -3);
        glTexCoord2f(1,1); glVertex3f(c.x, c.y, -3);
        glTexCoord2f(0,1); glVertex3f(c.x-c.largura, c.y, -3);
    glEnd();
    glDisable(GL_BLEND);
}

void setEstadoComidaStory(){
    if (reiDaNoite.x>=c.x && reiDaNoite.x-reiDaNoite.largura>=c.x-c.largura && reiDaNoite.x-reiDaNoite.largura<=c.x || reiDaNoite.x-reiDaNoite.largura<=c.x-c.largura && reiDaNoite.x<=c.x && reiDaNoite.x>=c.x-c.largura)
        if(reiDaNoite.y>=c.y && reiDaNoite.y-reiDaNoite.altura>=c.y-c.altura && reiDaNoite.y-reiDaNoite.altura<=c.y || reiDaNoite.y-reiDaNoite.altura<=c.y-c.altura && reiDaNoite.y<=c.y && reiDaNoite.y>=c.y-c.altura || reiDaNoite.y>=c.y && reiDaNoite.y-reiDaNoite.altura<=c.y-c.altura)
            c.estado=1;
    if(reiDaNoite.y>=c.y && reiDaNoite.y-reiDaNoite.altura>=c.y-c.altura && reiDaNoite.y-reiDaNoite.altura<=c.y || reiDaNoite.y-reiDaNoite.altura<=c.y-c.altura && reiDaNoite.y<=c.y && reiDaNoite.y>=c.y-c.altura)
        if (reiDaNoite.x>=c.x && reiDaNoite.x-reiDaNoite.largura>=c.x-c.largura && reiDaNoite.x-reiDaNoite.largura<=c.x || reiDaNoite.x-reiDaNoite.largura<=c.x-c.largura && reiDaNoite.x<=c.x && reiDaNoite.x>=c.x-c.largura || reiDaNoite.x>=c.x && reiDaNoite.x-reiDaNoite.largura<=c.x-c.largura)
            c.estado=1;
    desenhaComidaStory();
}

void desenhaEndStoryMode()
{
    reiDaNoite.estadoMovimento=0;
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1,1,1,1);
    glBindTexture(GL_TEXTURE_2D,ENDSTORY);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,0); glVertex3f(0,0,-2);
        glTexCoord2f(1,0); glVertex3f(tamanhomaximo.x,0,-2);
        glTexCoord2f(1,1); glVertex3f(tamanhomaximo.x,tamanhomaximo.y,-2);
        glTexCoord2f(0,1); glVertex3f(0,tamanhomaximo.y,-2);
    glEnd();
    glDisable(GL_BLEND);
}

void desenhaStoryMode(){
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    desenhaBarreiras();
    if(reiDaNoite.x<=(larguraBarreiras+reiDaNoite.largura) || reiDaNoite.x>=(100-larguraBarreiras) || reiDaNoite.y<=(larguraBarreiras+reiDaNoite.altura) || reiDaNoite.y>=(100-larguraBarreiras))
    {
        if(controlaStory==0)
            controlaLimite=0;
        else
            desenhaEndStoryMode();
    }
    if(jogoComecou)
        desenhaTelaPontosStory();

        desenhaPersonagens();
        setEstadoComidaStory();

        if(controlaPausar==(-1))
            desenhaTelaPause();
        if(controlaReiniciar==(-1))
            desenhaTelaReiniciar();
        if(controlaSairJogo)
            desenhaTelaSair();
        if(controlaLimite==0)
            desenhaGameOver();


    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glutSwapBuffers();
}

void movimentoMouse(int x, int y) {
    posicaoMouse.x = x;
    posicaoMouse.y = y;
}

void clickMouse(int button, int state, int x, int y){
    posicaoMouse.x = x;
    posicaoMouse.y = y;
    if(button == GLUT_LEFT_BUTTON)
        if(state == GLUT_DOWN){
            if(posicaoMouse.x>=telaStoryMode.x && posicaoMouse.y >=telaStoryMode.y && posicaoMouse.x <= telaStoryMode.x+telaStoryMode.largura && posicaoMouse.y<= telaStoryMode.y+telaStoryMode.altura){
                telaAtualDoJogo=4;
                glutDisplayFunc(desenhaStoryMode);
            }
            else if (posicaoMouse.x>=telaTimeTrial.x && posicaoMouse.y >=telaTimeTrial.y && posicaoMouse.x <= telaTimeTrial.x+telaTimeTrial.largura && posicaoMouse.y<= telaTimeTrial.y+telaTimeTrial.altura){
                telaAtualDoJogo=3;
                glutDisplayFunc(desenhaTimeTrial);
            }
            else if (posicaoMouse.x>=telaHowToPlay.x && posicaoMouse.y >=telaHowToPlay.y && posicaoMouse.x <= telaHowToPlay.x+telaHowToPlay.largura && posicaoMouse.y<= telaHowToPlay.y+telaHowToPlay.altura){
                telaAtualDoJogo=1;
                glutDisplayFunc(desenhaInstrucoes);
            }
            else if (posicaoMouse.x>=telaCreditos.x && posicaoMouse.y >=telaCreditos.y && posicaoMouse.x <= telaCreditos.x+telaCreditos.largura && posicaoMouse.y<= telaCreditos.y+telaCreditos.altura){
                telaAtualDoJogo=2;
                glutDisplayFunc(desenhaCreditos);
            }
            else{
                glutPostRedisplay();
            }
        }
}

void setasTeclado(int key, int x, int y)
{
    if(key==GLUT_KEY_UP)
    reiDaNoite.estadoMovimento=1;
    else if(key==GLUT_KEY_DOWN)
    reiDaNoite.estadoMovimento=3;
    else if(key==GLUT_KEY_LEFT)
    reiDaNoite.estadoMovimento=4;
    else if(key==GLUT_KEY_RIGHT)
    reiDaNoite.estadoMovimento=2;
}

void teclaPressionada(unsigned char key, int x, int y)
{
    switch(key)
    {
    case 't':
    case 'T':
        if(telaAtualDoJogo==0)
        {
            telaAtualDoJogo=3;
            glutDisplayFunc(desenhaTimeTrial);
        }
        break;
    case 'i':
    case 'I':
        if(telaAtualDoJogo==0)
        {
            telaAtualDoJogo=1;
            glutDisplayFunc(desenhaInstrucoes);
        }
        break;
    case 'h':
    case 'H':
        if(telaAtualDoJogo==0)
        {
            telaAtualDoJogo=4;
            glutDisplayFunc(desenhaStoryMode);
        }
        break;
    case 'c':
    case 'C':
        if(telaAtualDoJogo==0)
        {
            telaAtualDoJogo=2;
            glutDisplayFunc(desenhaCreditos);
        }
        break;
    case 'm':
    case 'M':
        if(telaAtualDoJogo==3 || telaAtualDoJogo==4)
        {
            reiDaNoite.estadoMovimento=0;
            glutDisplayFunc(desenhaMenuPrincipal);
            glutPostRedisplay();
            break;
        }
        else
        {
            telaAtualDoJogo=0;
            glutDisplayFunc(desenhaMenuPrincipal);
        }
        break;
    case 27:
        controlaSairJogo=1;
        reiDaNoite.estadoMovimento=0;
        glutPostRedisplay();
        break;
    case 'y':
    case 'Y':
        if(controlaSairJogo){
            // Limpando os ponteiros.
            Mix_FreeMusic(musicGame);
            // Fechando o SDL_mixer
            Mix_CloseAudio();
            exit(0);
        }
        else
            {
                reinicia();
                telaAtualDoJogo=0;
                glutDisplayFunc(desenhaMenuPrincipal);
                glutPostRedisplay();
            }
            break;
    case 'n':
    case 'N':
        if (controlaLimite==0 || controlaReiniciar==(-1))
        {
            controlaReiniciar=controlaReiniciar*(-1);
            controlaPausar=1;
            reiDaNoite.estadoMovimento=0;
            glutPostRedisplay();
        }
        else
        {
            controlaPausar=-1;
            controlaSairJogo=0;
            if(telaAtualDoJogo==3)
                glutDisplayFunc(desenhaTimeTrial);
            else if(telaAtualDoJogo==4);
                glutDisplayFunc(desenhaStoryMode);
        }
        break;
    case 'w':
    case 'W':
        if(telaAtualDoJogo==3 || telaAtualDoJogo==4) //se o jogo estiver em alguma das telas de jogo,
            if(jogoComecou==0)//e for o primeiro movimento executado
            {
                jogoComecou=1;//jogo comecou e o tempo reseta.
                inicioTempoJogo = time(NULL);
            }
            if(controlaPausar==1 && controlaReiniciar==1){
                if(controlaLimite) //somente permite andar se a variável controlaLimite nao tiver sido alterada para zero.
                {
                    reiDaNoite.estadoMovimento=1;
                    glutPostRedisplay();
                }}
        break;
    case 's':
    case 'S':
        if(telaAtualDoJogo==3 || telaAtualDoJogo==4)
            if(jogoComecou==0)
            {
                jogoComecou=1;
                inicioTempoJogo = time(NULL);
            }
            if(controlaPausar==1 && controlaReiniciar==1){
                if(controlaLimite)
                {
                    reiDaNoite.estadoMovimento=3;
                    glutPostRedisplay();
                }}
            else if (controlaReiniciar==(-1) || controlaLimite==0)
            {
                    reinicia();
                    glutPostRedisplay();
            }
        break;
    case 'a':
    case 'A':
        if(telaAtualDoJogo==3 || telaAtualDoJogo==4)
            if(jogoComecou==0)
            {
                jogoComecou=1;
                inicioTempoJogo = time(NULL);
            }
            if(controlaPausar==1 && controlaReiniciar==1){
                if(controlaLimite)
                {
                    reiDaNoite.estadoMovimento=4;
                    glutPostRedisplay();
                }}
        break;
    case 'D':
    case 'd':
        if(telaAtualDoJogo==3 || telaAtualDoJogo==4)
            if(jogoComecou==0)
            {
                jogoComecou=1;
                inicioTempoJogo = time(NULL);
            }
            if(controlaPausar==1 && controlaReiniciar==1){
                if(controlaLimite)
                {
                    reiDaNoite.estadoMovimento=2;
                    glutPostRedisplay();
                }}
        break;
    case 'r':
    case 'R':
        if(controlaLimite==0)
        {
            reinicia();
            glutPostRedisplay();
        }
        else if(controlaPausar==1)
        {
            controlaReiniciar=controlaReiniciar*(-1);
            reiDaNoite.estadoMovimento=0;
            glutPostRedisplay();
        }
        break;
    case 'p': // esta é a parte do código que altera o valor da variavel controlaPausar. 1 significa nao pausado, -1 significa pausado
    case 'P':
        controlaPausar=controlaPausar*(-1);
        reiDaNoite.estadoMovimento=0;
        glutPostRedisplay(); // este redisplay é para desenhar ou tirar da tela a tela de pause, atraves do desenhaMinhaCena
        break;
    }
}

void setup()
{
    glClearColor(0.0, 0.5, 0.8, 1.0);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    //definicao das texturas
    REIDANOITE = SOIL_load_OGL_texture("images/reidanoite.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    MURALHA = SOIL_load_OGL_texture("images/muralha.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    CHAO = SOIL_load_OGL_texture("images/nevenochao.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    //texturas das comidas
    BENJEN = SOIL_load_OGL_texture("images/benjen.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    OBERYN = SOIL_load_OGL_texture("images/oberyn.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    NED = SOIL_load_OGL_texture("images/ned.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    JEOR = SOIL_load_OGL_texture("images/jeor.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    DROGO = SOIL_load_OGL_texture("images/drogo.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    MINDINHO = SOIL_load_OGL_texture("images/mindinho.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    OLLY = SOIL_load_OGL_texture("images/olly.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    ROBB = SOIL_load_OGL_texture("images/robbstark.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    ROBERT = SOIL_load_OGL_texture("images/robert.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    TYWIN = SOIL_load_OGL_texture("images/tywin.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    YOUKNOWNOTHING = SOIL_load_OGL_texture("images/youknownothing.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    VISERYS = SOIL_load_OGL_texture("images/viserys.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    GHOST = SOIL_load_OGL_texture("images/ghost.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    LEAF = SOIL_load_OGL_texture("images/leaf.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    HODOR = SOIL_load_OGL_texture("images/hodor.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    VISERION = SOIL_load_OGL_texture("images/viserion.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    //texturas dos colecionaveis
    CORRENTES = SOIL_load_OGL_texture("images/chains.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    LANCADEGELO = SOIL_load_OGL_texture("images/icespear.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    RAMSAY = SOIL_load_OGL_texture("images/ramsay.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    //texturas das telas
    TELAPAUSE = SOIL_load_OGL_texture("images/telapause.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    TELAREINICIAR = SOIL_load_OGL_texture("images/telareiniciar.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    TELASAIR = SOIL_load_OGL_texture("images/telasair.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    SCORE = SOIL_load_OGL_texture("images/score.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    MENUPRINCIPAL = SOIL_load_OGL_texture("images/menuInicial.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    MENUPRINCIPALHTP = SOIL_load_OGL_texture("images/menuInicial_howToPlay.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    MENUPRINCIPALOPT = SOIL_load_OGL_texture("images/menuInicial_options.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    MENUPRINCIPALST = SOIL_load_OGL_texture("images/menuInicial_story.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    MENUPRINCIPALTT = SOIL_load_OGL_texture("images/menuInicial_timeTrial.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    HOWTOPLAY = SOIL_load_OGL_texture("images/howtoplay.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    CREDITOS = SOIL_load_OGL_texture("images/creditos.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    RETURNTOMENU = SOIL_load_OGL_texture("images/returntomenu.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    GAMEOVER = SOIL_load_OGL_texture("images/gameover.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    ENDSTORY = SOIL_load_OGL_texture("images/endstory.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    TIME = SOIL_load_OGL_texture("images/time.png",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);

    glEnable(GL_BLEND); // Habilitar a transparência.
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_BLEND);
}

void redimensionada(int width, int height){
   glViewport(0, 0, width, height);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(0.0, tamanhomaximo.x, 0.0, tamanhomaximo.y, tamanhomaximo.zmin, tamanhomaximo.zmax); // Começamos com z de -2 a 2

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

// Funcao que printa no terminal o tempo em segundos decorrido do game.
void getTempoGame(){
    deltaTempo=time(NULL)-inicioTempoJogo;
    m=(deltaTempo/60)%60;
    s=deltaTempo%60;
    // Funcao para chamar nossa função de tempo.
    glutTimerFunc(100,getTempoGame,1);
}
void controlaTempoDasParadinhas()
{
    deltaTempoParadinhas=time(NULL)-inicioTempoParadinhas;
    tempoParadinhas=deltaTempoParadinhas%60;
    if(controlaSubVelocidade==1){
        if(tempoParadinhas==tempoDuracaoSubVelocidade)
            controlaSubVelocidade=-1;
        }
    if(controlaSuperVelocidade==1){
        if(tempoParadinhas==tempoDuracaoSuperVelocidade)
            controlaSuperVelocidade=-1;
        }
    if(controlaInvisibilidade==1){
        if(tempoParadinhas==tempoDuracaoInvisibilidade)
            controlaInvisibilidade=-1;
        }
    glutTimerFunc(100,controlaTempoDasParadinhas,1);
}
void atualizaCena(){
    if(controlaLimite)
    {
        if(reiDaNoite.estadoMovimento==1)
        {
            if(controlaSubVelocidade==1)
                reiDaNoite.y+=0.3;
            else if(controlaSuperVelocidade==1)
                reiDaNoite.y+=2;
            else
                reiDaNoite.y++;
        }
        else if(reiDaNoite.estadoMovimento==2)
        {
            if(controlaSubVelocidade==1)
                reiDaNoite.x+=0.3;
            else if(controlaSuperVelocidade==1)
                reiDaNoite.x+=2;
            else
                reiDaNoite.x++;
        }
        else if(reiDaNoite.estadoMovimento==3)
        {
            if(controlaSubVelocidade==1)
                reiDaNoite.y-=0.3;
            else if(controlaSuperVelocidade==1)
                reiDaNoite.y-=2;
            else
                reiDaNoite.y--;
        }
        else if(reiDaNoite.estadoMovimento==4)
        {
            if(controlaSubVelocidade==1)
                reiDaNoite.x-=0.3;
            else if(controlaSuperVelocidade==1)
                reiDaNoite.x-=2;
            else
                reiDaNoite.x--;
        }
        glutPostRedisplay();
    }
}

int main(int argc, char** argv){
    int i;
    srand(time(0));
    inicioTempoJogo = time(NULL);
    inicioTempoParadinhas = time(NULL);

    // Inicializando SDL.
    if(SDL_Init(SDL_INIT_AUDIO) < 0)
        return -1;

    // Inicializando o SDL_mixer
    if(Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
        return -1;

    // Carregando minha música.
    musicGame = Mix_LoadMUS(MUS_GAME_PATH);
    if(musicGame == NULL)
        return -1;

    if(Mix_PlayMusic(musicGame, -1) == -1)
        return -1;

    //PARTE DE INICIAÇÃO DE VARIÁVEIS
        // Personagem.
        reiDaNoite.largura = 6;
    	reiDaNoite.altura = 6;
    	reiDaNoite.x = 50;
    	reiDaNoite.y = 50;
        reiDaNoite.estadoMovimento = 0;
        reiDaNoite.transparencia = 1;

        // Comida.
        c.largura=6;
        c.altura=6;
        c.estado=1; // Comida normal.

        //Tamanho do mundo
        tamanhomaximo.x=100;
        tamanhomaximo.y=100;
        tamanhomaximo.zmin=-3;
        tamanhomaximo.zmin=3;

        //Tela de pause
        telaPause.x=50;
        telaPause.y=50;
        telaPause.largura= 85;
        telaPause.altura= 125;
        telaPause.transparencia=1;

        telaStoryMode.x=305;
        telaStoryMode.y=254;
        telaStoryMode.largura=196;
        telaStoryMode.altura=76;

        telaTimeTrial.x=telaStoryMode.x;
        telaTimeTrial.largura=telaStoryMode.largura;
        telaTimeTrial.altura=telaStoryMode.altura;
        telaTimeTrial.y=368;

        telaHowToPlay.x=telaStoryMode.x;
        telaHowToPlay.largura=telaStoryMode.largura;
        telaHowToPlay.altura=telaStoryMode.altura;
        telaHowToPlay.y=488;

        telaCreditos.x=telaStoryMode.x;
        telaCreditos.largura=telaStoryMode.largura;
        telaCreditos.altura=telaStoryMode.altura;
        telaCreditos.y=598;
        // Tela de pontos
        telaPontos.x=86; // Canto superior direito da tela.
        telaPontos.y=96;
        telaPontos.largura=15;
        telaPontos.altura=8;
        telaPontos.transparencia=1;

        telaPontosStory.x=92;
        telaPontosStory.y=94;
        telaPontosStory.largura=15;
        telaPontosStory.altura=25;
        telaPontosStory.transparencia=1;

        //tela de reiniciar
        telaReiniciar.x=50;
        telaReiniciar.y=50;
        telaReiniciar.largura=telaPause.largura;
        telaReiniciar.altura=telaPause.altura;
        telaReiniciar.transparencia=telaPause.transparencia;


   glutInit(&argc, argv);
   // Funcao para chamar nossa função de tempo.
   glutTimerFunc(20,getTempoGame,1);
   glutTimerFunc(20,controlaTempoDasParadinhas,1);
   glutInitContextVersion(1, 1);
   glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
   glutInitWindowSize(800, 700);
   glutInitWindowPosition(100, 100);
   glutCreateWindow("GAME OF Game of Thrones");
   glutSpecialFunc(setasTeclado);
   glutKeyboardFunc(teclaPressionada);
   glutPassiveMotionFunc(movimentoMouse);
   glutMouseFunc(clickMouse);
   glutIdleFunc(atualizaCena);
   /*
       tela 0 = menu inicial
       tela 1 = instrucoes
       tela 2 = creditos
       tela 3 = jogar (time trial)
   */
    if(telaAtualDoJogo==0)
        glutDisplayFunc(desenhaMenuPrincipal);
    else if(telaAtualDoJogo==1)
        glutDisplayFunc(desenhaInstrucoes);
    else if(telaAtualDoJogo==2)
        glutDisplayFunc(desenhaCreditos);
    else if(telaAtualDoJogo==3)
        glutDisplayFunc(desenhaTimeTrial);
    glutKeyboardFunc(teclaPressionada);
    glutReshapeFunc(redimensionada);

   setup();
   if(!Mix_PlayingMusic()){
       if(Mix_PlayMusic(musicGame, -1) == -1)
           return -1;
   }
   glutMainLoop();
   return 0;
}
