# Tp 1 Computação Gráfica: Game of Game Of Thrones
# Integrantes: **Daniel Santana** e **Isaque Fernando**

## Informações Importantes:
    Dependências:
        libSoil
        libGLU
        libGLEW
        libfreeglut
        OpenGL
        libsdl2_mixer
        libsdl2
    Instalação da SDL2:
        Em seu terminal execute:
        ```bash
        $ sudo apt-get install freeglut3 freeglut3-dev binutils-gold g++ cmake libglew-dev mesa-common-dev build-essential libglew1.5-dev libglm-dev libsdl2-mixer-dev
        ```

    Para executar o game:
        Compilar: make compile
        Executar: make run
        Limpar Arquivos Temporários: make clean

## Descrição do Game:
    Game no estilo Agar.io com o tema do Game Of Thrones. O game possui dois modos de jogo: TimeTrial, onde você tem 30 segundos para fazer a maior pontuação possível, e um Story Mode, onde você tem que coletar 5 colecionáveis,dominar o dragão e passar pela muralha(que ficará vermelha) para vencer.

**Já avisamos sobre possíveis SPOILERS da setima temporada da série.**

## Recursos básicos do game:
    Câmera em um ambiente 2D.
    Personagem controlável pelas setas ou 'WASD'.
    Mundo finito.
    Surgimento de paradas coletáveis em posições aleatórias.
    Existência de paredes, que matem o personagem e façam o jogo reiniciar.
    Comandos básicos do jogo via teclado:
        encerrar quando a tecla 'Esc' for pressionada.
        reiniciar quando a tecla 'r' for pressionada.
        pausar quando a tecla 'p' for pressionada.

## Recursos adicionais implementados:
    Itens com poderes (duração 5 segundos):
        A personagem Hodor diminui a velocidade do jogador pela metade.
        A personagem Ghost dobra a velocidade do jogador.
        A personagem Leaf deixa o personagem "invisível".
    Texturas:
        Todo o jogo está texturizado.
        Todo o menu foi criado por nós(com a ajuda do César).
        O Photoshop foi utilizado na edição das texturas usadas.
    Navegação:
        O menu pode ser controlado pelo mouse assim como pelos atalhos de teclado. Os atalhos são: H - Modo história, T - Time Trial, I - Como Jogar e C - Créditos e M - Retorna ao Menu.
    HUD:
        Pontuação sendo exibida em cada modo do jogo.
        A duração dos "poderes" também é exibida em ambos os modos, O tempo restante é exibido no modo Time Trial e os coletáveis já adquiridos no modo História também são exibidos.
    Outras Funcionalidades:
        Música de Fundo com o tema da série.
        Diversas telas compõem o game, como Game Over, Reinicio, Menu etc.
    Implementação Criativa:
        Modo história com **SPOILER** da série.(Difícil pra burro..).
